This is a (yet another) (very) basic [[https://www.mpi-forum.org/][MPI]] [[https://ftp.gnu.org/gnu/hello/][Hello World]] (in C++ and using
[[https://cmake.org/][CMake]]) based on [[https://mpitutorial.com/tutorials/mpi-hello-world/][the mpitutorial]] whose source can be browsed [[./hello-mpi.cpp][here]]
(simple MPI hello world) and [[./hello-mpi-barrier.cpp][there]] (simple MPI barrier). Each MPI
process shall display its own =rank= and =size= of the
=MPI_COMM_WORLD=, as well as the [[https://www.open-mpi.org/doc/v3.1/man3/MPI_Get_processor_name.3.php][processor name]].

#+begin_src bash :eval no
  mkdir build
  cd build
  cmake ..
  make
#+end_src

Then ~mpirun -n 2 ./hello-mpi~ shall display something like:

#+begin_example
MPI Hello World from process rank 0 out of 2 on machine0.
MPI Hello World from process rank 1 out of 2 on machine1.
#+end_example

And ~mpirun -n 2 ./hello-mpi-barrier~ shall display something like:

#+begin_example
MPI process 1 on machine1 out of 2 processes is entering the barrier.
MPI process 0 on machine0 out of 2 processes is entering the barrier.
All processes have reached the barrier.
#+end_example

This might be useful for quick troubleshooting when wondering whether
processes are bound as expected.
