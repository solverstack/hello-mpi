#include <iostream>
#include <mpi.h>

int main(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    char processor_name[MPI_MAX_PROCESSOR_NAME];
    int name_len;
    MPI_Get_processor_name(processor_name, &name_len);

    std::cout << "MPI process " << rank << " on " << processor_name
              << " out of " << size << " processes is entering the barrier." << std::endl;

    MPI_Barrier(MPI_COMM_WORLD);

    if (rank == 0) {
        std::cout << "All processes have reached the barrier." << std::endl;
    }

    MPI_Finalize();
    return 0;
}
