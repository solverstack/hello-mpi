cmake_minimum_required(VERSION 3.12)

project(helloworld VERSION 1.0.0 LANGUAGES C CXX)

# to be able to use "PACKAGE"_ROOT env. var.
cmake_policy(SET CMP0074 NEW)
# to be able to use target_link_libraries on targets not defined on the same scope
cmake_policy(SET CMP0079 NEW)
# For fPIC when build static
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

#######
# MPI #
#######

find_package(MPI)
if(NOT MPIEXEC_EXECUTABLE)
  set(MPIEXEC ${MPIEXEC_EXECUTABLE})
  if(NOT MPIEXEC_EXECUTABLE)
    set(MPIEXEC_EXECUTABLE "mpirun")
  endif()
endif()

# target_link_libraries(${CMAKE_PROJECT_NAME} INTERFACE MPI::MPI_CXX)

##########
# OpenMP #
##########

# For hello-mpi-threads and hello-mpi-threads-hwloc

# Enable OpenMP
find_package(OpenMP REQUIRED)

# Find Hwloc (as target PkgConfig::HWLOC)
include(FindPkgConfig)
if(PKG_CONFIG_FOUND)
  pkg_search_module(HWLOC REQUIRED IMPORTED_TARGET hwloc)
  if(HWLOC_MODULE_NAME)
    message(STATUS "Found ${HWLOC_MODULE_NAME}")
  else()
    message(FATAL_ERROR "Looking for HWLOC - not found using PkgConfig."
    "\n   You should add the directory containing hwloc.pc to the PKG_CONFIG_PATH environment variable.")
    endif()
else(PKG_CONFIG_FOUND)
  message(FATAL_ERROR "FindHWLOC needs pkg-config program and PKG_CONFIG_PATH must contain the path to hwloc.pc file.")
endif(PKG_CONFIG_FOUND)

####################
# Main executables #
####################

add_executable(hello-mpi hello-mpi.cpp)
target_link_libraries(hello-mpi PUBLIC MPI::MPI_CXX)

add_executable(hello-mpi-barrier hello-mpi-barrier.cpp)
target_link_libraries(hello-mpi-barrier PUBLIC MPI::MPI_CXX)

add_executable(hello-mpi-threads hello-mpi-threads.cpp)
target_link_libraries(hello-mpi-threads PUBLIC MPI::MPI_CXX OpenMP::OpenMP_CXX)

add_executable(hello-mpi-threads-hwloc hello-mpi-threads-hwloc.cpp)
target_link_libraries(hello-mpi-threads-hwloc PUBLIC MPI::MPI_CXX OpenMP::OpenMP_CXX PkgConfig::HWLOC)

#########
# Tests #
#########

enable_testing()

add_test(test-hello-mpi mpiexec -n 2 ./hello-mpi)

add_test(test-hello-mpi-barrier mpiexec -n 2 ./hello-mpi-barrier)

add_test(test-hello-mpi-threads mpiexec -n 2 ./hello-mpi-threads)
set_tests_properties(test-hello-mpi-threads PROPERTIES
  ENVIRONMENT "OMP_NUM_THREADS=2"
)

add_test(test-hello-mpi-threads-hwloc mpiexec -n 2 ./hello-mpi-threads-hwloc)
set_tests_properties(test-hello-mpi-threads-hwloc PROPERTIES
  ENVIRONMENT "OMP_NUM_THREADS=2"
)


###########
# Install #
###########

install(TARGETS hello-mpi
  RUNTIME DESTINATION bin)

install(TARGETS hello-mpi-barrier
  RUNTIME DESTINATION bin)

install(TARGETS hello-mpi-threads
  RUNTIME DESTINATION bin)

install(TARGETS hello-mpi-threads-hwloc
  RUNTIME DESTINATION bin)
