#include <mpi.h>
#include <omp.h>
#include <iostream>
#include <sched.h>  // For sched_getcpu()

int main(int argc, char *argv[]) {
    int provided;

    // Initialize MPI with support for threads
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);

    if (provided < MPI_THREAD_MULTIPLE) {
        std::cerr << "MPI_THREAD_MULTIPLE support is not available." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
    }

    int rank, size;

    // Get the rank and size of the MPI communicator
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    // OpenMP parallel region
    #pragma omp parallel
    {
        int thread_id = omp_get_thread_num();
        int num_threads = omp_get_num_threads();

        // Get the CPU core ID for the current thread
        int cpu_core_id = sched_getcpu();

        // Print a message from each thread
        #pragma omp critical
        std::cout << "Hello from MPI process " << rank << "/" << size
                  << ", thread " << thread_id << "/" << num_threads
                  << ", CPU core " << cpu_core_id << std::endl;
    }

    // Finalize MPI
    MPI_Finalize();

    return 0;
}
