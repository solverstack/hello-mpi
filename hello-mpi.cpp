#include <iostream>
#include <mpi.h>

int main(int argc, char **argv)
{
  int world_size, world_rank, processor_name_length;
  char processor_name[MPI_MAX_PROCESSOR_NAME];

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  MPI_Get_processor_name(processor_name, &processor_name_length);

  std::cout << "MPI Hello World from process rank " << world_rank << " out of " << world_size << " on " << std::string(processor_name) << "." << "\n";

  MPI_Finalize();

  return 0;
}

