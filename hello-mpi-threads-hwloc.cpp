#include <mpi.h>
#include <omp.h>
#include <iostream>
#include <cstring>  // For strerror
#include <cstdlib>  // For EXIT_FAILURE
#include <unistd.h> // For gethostname
#include <sched.h>  // For sched_getcpu()
#include <hwloc.h>  // Include hwloc header


void print_hwloc_info(hwloc_topology_t topo, int cpu_core_id) {
    hwloc_obj_t obj = hwloc_get_obj_by_type(topo, HWLOC_OBJ_PU, cpu_core_id);
    if (obj == nullptr) {
        std::cerr << "Error: Couldn't find the PU object for CPU core " << cpu_core_id << std::endl;
        return;
    }

    // Print the logical core ID
    int logical_core_id = obj->logical_index;
    std::cout << "Logical Core ID: " << logical_core_id << std::endl;

    // Get socket info
    hwloc_obj_t socket = hwloc_get_ancestor_obj_by_type(topo, HWLOC_OBJ_SOCKET, obj);
    if (socket) {
        std::cout << "Socket: " << socket->logical_index << std::endl;
    } else {
        std::cout << "Socket: Not found" << std::endl;
    }

    // Get NUMA node info
    hwloc_obj_t numa_node = hwloc_get_ancestor_obj_by_type(topo, HWLOC_OBJ_NUMANODE, obj);
    if (numa_node) {
        std::cout << "NUMA Node: " << numa_node->logical_index << std::endl;
    } else {
        // Handle single NUMA node case
        std::cout << "NUMA Node: Not applicable (single NUMA node system)" << std::endl;
    }
}

int main(int argc, char *argv[]) {
    int provided;

    // Initialize MPI with support for threads
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);

    if (provided < MPI_THREAD_MULTIPLE) {
        std::cerr << "MPI_THREAD_MULTIPLE support is not available." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
    }

    int rank, size;

    // Get the rank and size of the MPI communicator
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    // Initialize hwloc
    hwloc_topology_t topo;
    hwloc_topology_init(&topo);
    hwloc_topology_load(topo);

    // Hostname
    char hostname[256]; // Buffer to store the hostname
    if (gethostname(hostname, sizeof(hostname)) != 0) {
        std::cerr << "Error getting hostname: " << strerror(errno) << std::endl;
        exit(EXIT_FAILURE);
    }

    // OpenMP parallel region
    #pragma omp parallel
    {
        int thread_id = omp_get_thread_num();
        int num_threads = omp_get_num_threads();

        // Get the CPU core ID for the current thread using sched_getcpu
        int cpu_core_id = sched_getcpu();

        // Print detailed information using hwloc
        #pragma omp critical
        {
          std::cout << rank << ":" << thread_id << ": Hello from MPI process " << rank << "/" << size
                            << " on " << hostname
			    << ", thread " << thread_id << "/" << num_threads
			    << ", CPU core (sched_getcpu) " << cpu_core_id << std::endl;

            // Call the function to print detailed hardware information
            print_hwloc_info(topo, cpu_core_id);
        }
    }

    // Finalize MPI
    MPI_Finalize();

    // Cleanup hwloc
    hwloc_topology_destroy(topo);

    return 0;
}
